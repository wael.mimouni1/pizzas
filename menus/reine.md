# Pizza Reine

## Ingrédients
 - sauce tomates
 - champignons de Paris
 - jambon
 - gruyère
 - sel
 - poivre

## Allergènes connus
 - Gluten
 - Traces de fruits à coque et de noix
 - Lactose
